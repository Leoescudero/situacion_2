package situacion_2;

import org.junit.Test;
import static org.junit.Assert.*;

public class SocioTest {
    
    @Test public void pagoSocioVitalicio() {

        Socio socio = new Socio();
        socio.setNombre("Francisco");
        socio.setApellido("Guerra");
        socio.setDocumento(41654872);
        socio.setAnioAntiguedad(25);
        socio.setMesAntiguedad(3);
        socio.setVitalicio(true);

        Cuota pago1 = new Cuota();
        assertTrue(pago1.generarComprobanteMensual(socio));
        assertFalse(pago1.generarComprobantePago(socio));
        assertEquals(0,pago1.realizarPago(socio));

    
    }

}
