package situacion_2;

public class Principal {
    
    public static void main(String[] args) {
        
        Socio socio = new Socio();

        socio.setNombre("Francisco");
        socio.setApellido("Guerra");
        socio.setDocumento(41654872);
        socio.setAnioAntiguedad(25);
        socio.setMesAntiguedad(3);

        Cuota pago1 = new Cuota();

        if (socio.getVitalicio() == true)
        {
            if (pago1.generarComprobanteMensual(socio) == true)
            {
                System.out.println("\nComprobrante generado exitosamente!");
            }
        }
        else
        {
            if (pago1.generarComprobantePago(socio) == true)
            {
                System.out.println("\nPago realizado correctamente!");
            }
        }


    }

}
