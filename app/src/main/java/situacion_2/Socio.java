package situacion_2;
public class Socio {
    private String nombre;
    private String apellido;
    private int documento;
    private Integer anioAntiguedad;
    private Integer mesAntiguedad;
    private boolean vitalicio;
    public String getNombre(){
        return nombre;
    }
    public void setNombre(String nombreSocio){
        this.nombre=nombreSocio;
    }
    public String getApellido(){
        return apellido;
    }
    public void setApellido(String apellidoSocio){
        this.apellido=apellidoSocio;
    }
    public int getDocumento(){
        return documento;
    }
    public void setDocumento(int documentoSocio){
        this.documento=documentoSocio;
    }
    public void setAnioAntiguedad(Integer anioAntiguedad){
        this.anioAntiguedad = anioAntiguedad;
    }
    public Integer getAnioAntiguedad(){
        return anioAntiguedad;
    }
    public void setVitalicio(boolean vitalicio)
    {
        this.vitalicio = vitalicio;
    }
    public boolean getVitalicio()
    {
        return vitalicio;
    }
    public void setMesAntiguedad(Integer mesAntiguedad){
        this.mesAntiguedad=mesAntiguedad;
    }
    public Integer getMesAntiguedad()
    {
        return mesAntiguedad;
    }

}
